package feedback;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedback.model.Question;

@RestController
@RequestMapping("/users")
public class QuestionController {

	@RequestMapping(method=RequestMethod.GET, value="")
	public List<Question> index () {
		// TODO Auto-generated constructor stub
		return null;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{questionId}")
	public Question getQuestion (@PathVariable("questionId") Integer questionId) {
		// TODO Auto-generated constructor stub
		return null;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="")
	public Question create () {
		// TODO Auto-generated constructor stub
		return null;
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{questionId}")
	public Question update(@PathVariable("questionId") Integer questionId) {
		// TODO Auto-generated constructor stub
		return null;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/")
	public void delete(@PathVariable("questionId") Integer questionId) {
		// TODO Auto-generated constructor stub
	}
	
}
