package feedback.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// Generated May 6, 2015 5:52:46 PM by Hibernate Tools 4.3.1

/**
 * TblSurveys generated by hbm2java
 */
@Entity
@Table(name = "tblSurveys")
public class Surveys implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	
	@OneToMany(mappedBy="survey", fetch=FetchType.LAZY)
	private List<Question>questions;

	public Surveys() {
	}

	public Surveys(String name) {
		this.name = name;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
