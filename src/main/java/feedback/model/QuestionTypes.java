package feedback.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

// Generated May 6, 2015 5:52:46 PM by Hibernate Tools 4.3.1

/**
 * TblQuestionTypes generated by hbm2java
 */
@Entity
@Table(name = "tblQuestionTypes")
public class QuestionTypes implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	
	@OneToOne(mappedBy="questionType", fetch=FetchType.LAZY)
	private Question question;

	public QuestionTypes() {
	}

	public QuestionTypes(String name) {
		this.name = name;
	}

	public long getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
