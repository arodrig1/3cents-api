package feedback.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

// Generated May 6, 2015 5:52:46 PM by Hibernate Tools 4.3.1

/**
 * TblQuestionOptions generated by hbm2java
 */
@Entity
@Table(name = "tblQuestionOptions")
public class QuestionOptions implements java.io.Serializable, Comparable<QuestionOptions> {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
    @Column(insertable=false, updatable=false)
	private Integer questionId;
	private String optionText;
	private String optionValue;
	private Integer optionOrder;
	private String optionType;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="questionId")
	private Question question;

	public QuestionOptions() {
	}

	public QuestionOptions(Integer questionId, String optionText,
			String optionValue, Integer optionOrder) {
		this.questionId = questionId;
		this.optionText = optionText;
		this.optionValue = optionValue;
		this.optionOrder = optionOrder;
	}

	
	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public long getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getOptionText() {
		return this.optionText;
	}

	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	public String getOptionValue() {
		return this.optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public Integer getOptionOrder() {
		return this.optionOrder;
	}

	public void setOptionOrder(Integer optionOrder) {
		this.optionOrder = optionOrder;
	}

	@Override
	public int compareTo(QuestionOptions o) {
		
		return Integer.compare(this.optionOrder, o.optionOrder);
	}

}
