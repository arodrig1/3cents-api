package feedback.service;

import feedback.model.SurveyInstances;
import feedback.requests.AnswerBatchRequest;
import feedback.response.SurveyInstanceAnswerResponse;
import feedback.response.SurveyInstanceQuestionResponse;
import feedback.response.SurveyRecipientResponse;

public interface SurveyServiceExternal {

	public abstract SurveyInstances getSurveyInstance(Integer id);

	public abstract SurveyRecipientResponse getSurveyRecipients(Integer surveyId);

	public abstract SurveyInstanceQuestionResponse getSurveyQuestions(
			Integer surveyId);

	public abstract SurveyInstanceAnswerResponse getSurveyAnswers(
			Integer surveyId);

	public abstract void updateAnswers(AnswerBatchRequest answerRequest,
			Integer recipientId, Integer userId, Integer surveyInstanceId);

	public abstract void createSurveyRequest(Integer userId,
			Integer recipientId, Integer surveyInstanceId);

}