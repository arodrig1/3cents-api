package feedback.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedback.model.Users;
import feedback.repository.UserRepository;

@Service
public class UserService implements UserServiceExternal {

	@Autowired
	UserRepository userRepo;
	
	/* (non-Javadoc)
	 * @see feedback.service.UserServiceExternal#testing()
	 */
	@Override
	public String testing(){
		Users user = userRepo.findOne(1L);
		
		return user.getFullName();
	}
}
