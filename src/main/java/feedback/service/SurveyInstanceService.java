package feedback.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedback.model.Answers;
import feedback.model.Question;
import feedback.model.SurveyCompletions;
import feedback.model.SurveyInstances;
import feedback.model.SurveysInProgress;
import feedback.model.Users;
import feedback.repository.AnswersRepository;
import feedback.repository.SurveyInProgressRepository;
import feedback.repository.SurveyInstanceRepository;
import feedback.repository.SurveysCompletedRepository;
import feedback.requests.AnswerBatchRequest;
import feedback.requests.AnswerRequest;
import feedback.response.AnswerResponse;
import feedback.response.QuestionResponse;
import feedback.response.RecipientResponse;
import feedback.response.SurveyInstanceAnswerResponse;
import feedback.response.SurveyInstanceQuestionResponse;
import feedback.response.SurveyRecipientResponse;

@Service
public class SurveyInstanceService implements SurveyServiceExternal {

	@Autowired
	SurveyInstanceRepository surveyInstanceRepo;
	
	@Autowired
	SurveyInProgressRepository surveyInProgressRepo;
	
	@Autowired
	SurveysCompletedRepository surveyCompletedRepo;
	
	@Autowired
	AnswersRepository answerRepo;
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#getSurveyInstance(java.lang.Integer)
	 */
	@Override
	public SurveyInstances getSurveyInstance(Integer id){
		
		return surveyInstanceRepo.findOne(id.longValue());
	}
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#getSurveyRecipients(java.lang.Integer)
	 */
	@Override
	public SurveyRecipientResponse getSurveyRecipients(Integer surveyId){
		
		SurveyRecipientResponse response = new SurveyRecipientResponse();
		
		response.setId(surveyId);

		ArrayList<RecipientResponse> recipients = new ArrayList<RecipientResponse>();
		
		List<SurveysInProgress> inProgfromDatabase = surveyInProgressRepo.findBySurveyInstanceId(surveyId);
		Iterator<SurveysInProgress> inProgItr = inProgfromDatabase.iterator();
		while(inProgItr.hasNext()){
			RecipientResponse recipientResponse = new RecipientResponse(inProgItr.next());
			recipients.add(recipientResponse);
		}
		
		List<SurveyCompletions> completeDatabase = surveyCompletedRepo.findBySurveyInstanceId(surveyId);
		Iterator<SurveyCompletions> completeItr = completeDatabase.iterator();
		while(completeItr.hasNext()){
			RecipientResponse recipientResponse = new RecipientResponse(completeItr.next());
			recipients.add(recipientResponse);
		}
		
		
		return response;
	}
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#getSurveyQuestions(java.lang.Integer)
	 */
	@Override
	public SurveyInstanceQuestionResponse getSurveyQuestions(Integer surveyId){
		
		SurveyInstanceQuestionResponse response = new SurveyInstanceQuestionResponse();
		
		SurveyInstances surveyInstance = surveyInstanceRepo.findOne(surveyId.longValue());
		
		response.setId(surveyId);
		response.setStartTimeStamp(surveyInstance.getStartTimestamp().toString());
		response.setEndTimeStamp(surveyInstance.getEndTimestamp().toString());
		response.setName(surveyInstance.getSurvey().getName());
		
		List<Question> questions = surveyInstance.getQuestions();
		List<QuestionResponse> qResponses = new ArrayList<QuestionResponse>();
		Iterator<Question> questionItr = questions.iterator();
		while(questionItr.hasNext()){
			
			QuestionResponse qResponse = new QuestionResponse(questionItr.next());
			qResponses.add(qResponse);
			
		}
		response.setQuestions(qResponses);
		
		return response;
	}
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#getSurveyAnswers(java.lang.Integer)
	 */
	@Override
	public SurveyInstanceAnswerResponse getSurveyAnswers(Integer surveyId){
		
		SurveyInstanceAnswerResponse response = new SurveyInstanceAnswerResponse();
		
		SurveyInstances surveyInstance = surveyInstanceRepo.findOne(surveyId.longValue());
		response.setId(surveyId);
		response.setName(surveyInstance.getSurvey().getName());
		
		List<Question> questions = surveyInstance.getQuestions();
		List<AnswerResponse> aResponses = new ArrayList<AnswerResponse>();
		Iterator<Question> questionItr = questions.iterator();
		while(questionItr.hasNext()){
			
			Question question = questionItr.next();
			
			Answers answer = question.getAnswer();
			
			if(answer != null){
				AnswerResponse aResponse = new AnswerResponse(answer);
				
				aResponses.add(aResponse);
				
			}
			
		}
		response.setAnswers(aResponses);
		
		return response;
	}
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#updateAnswers(feedback.requests.AnswerBatchRequest, java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void updateAnswers(AnswerBatchRequest answerRequest, Integer recipientId, Integer userId, Integer surveyInstanceId){
		
		SurveysInProgress inProg = surveyInProgressRepo.findByUserIdAndRecipientIdAndSurveyInstanceId(userId, recipientId, surveyInstanceId);
		
		//had an entry
		if(inProg != null){
			
			//survey is done so delete
			if(answerRequest.getComplete()){
				surveyInProgressRepo.delete(inProg);
			}
			
		}
		//didn't have an entry
		else{
			//survey isn't done so create
			if(answerRequest.getComplete() == false){
				SurveysInProgress newInProg = new SurveysInProgress();
				
				newInProg.setWasRequested(false);
				newInProg.setSurveyInstanceId(surveyInstanceId);
				newInProg.setRecipientId(recipientId);
				
				Users user = new Users();
				
				user.setId(userId.longValue());
				newInProg.setUser(user);
				
				surveyInProgressRepo.save(newInProg);
			}
		}
		
		//this survey is done so make a new entry
		if(answerRequest.getComplete()){
			
			SurveyCompletions newCompletion = new SurveyCompletions();
			
			newCompletion.setWasRequested(false);
			newCompletion.setSurveyInstanceId(surveyInstanceId);
			newCompletion.setRecipientId(recipientId);
			
			Users user = new Users();
			
			user.setId(userId.longValue());
			newCompletion.setUser(user);
			
			surveyCompletedRepo.save(newCompletion);
		}
		
		List<AnswerRequest>answers = answerRequest.getAnswers();
		Iterator<AnswerRequest> answerItr = answers.iterator();
		while(answerItr.hasNext()){
			
			AnswerRequest aReq = answerItr.next();
			Answers answer = new Answers();
			
			answer.setUserId(userId);
			answer.setRecipientId(recipientId);
			answer.setSurveyInstanceId(surveyInstanceId);
			
			Question q = new Question();
			q.setId(aReq.getQuestionId().longValue());
			answer.setQuestion(q);
			
			answer.setAnswerText(aReq.getAnswerText());
			
			answerRepo.save(answer);
		}

		
	}
	
	/* (non-Javadoc)
	 * @see feedback.service.SurveyServiceExternal#createSurveyRequest(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void createSurveyRequest(Integer userId, Integer recipientId, Integer surveyInstanceId){
		
		SurveysInProgress inProgress = new SurveysInProgress(userId, recipientId, surveyInstanceId, true);
		
		surveyInProgressRepo.save(inProgress);
		
	}
	
	
	
}
