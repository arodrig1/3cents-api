package feedback.response;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;

import feedback.model.Question;
import feedback.model.QuestionOptions;

@JsonRootName("question")
public class QuestionResponse {

	private Integer id;
	private String text;
	private String rangeStart;
	private String rangeStop;
	private String questionType;
	
	private List<QuestionOptionResponse> options;
	
	
	
	public List<QuestionOptionResponse> getOptions() {
		return options;
	}
	public void setOptions(List<QuestionOptionResponse> options) {
		this.options = options;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRangeStart() {
		return rangeStart;
	}
	public void setRangeStart(String rangeStart) {
		this.rangeStart = rangeStart;
	}
	public String getRangeStop() {
		return rangeStop;
	}
	public void setRangeStop(String rangeStop) {
		this.rangeStop = rangeStop;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	
	public QuestionResponse(Question question){
		
		this.id = question.getId().intValue();
		this.text = question.getQuestionText();
		this.rangeStart = question.getQuestionRangeStart().toString();
		this.rangeStop = question.getQuestionRangeEnd().toString();
		this.questionType = question.getQuestionType().getName();
		List<QuestionOptions> options = question.getQuestionOptions();
		Iterator<QuestionOptions> optionItr = options.iterator();
		
		Collections.sort(options);
		while(optionItr.hasNext()){
			
			QuestionOptionResponse option = new QuestionOptionResponse(optionItr.next());
			this.options.add(option);
			
		}
		
	}
	
}
