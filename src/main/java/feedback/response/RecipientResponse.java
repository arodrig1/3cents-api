package feedback.response;

import feedback.model.SurveyCompletions;
import feedback.model.SurveysInProgress;

public class RecipientResponse {

	private Integer id;
	private String name;
	private Boolean complete;
	private Boolean wasRequested;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getComplete() {
		return complete;
	}
	public void setComplete(Boolean complete) {
		this.complete = complete;
	}
	public Boolean getWasRequested() {
		return wasRequested;
	}
	public void setWasRequested(Boolean wasRequested) {
		this.wasRequested = wasRequested;
	}
	
	public RecipientResponse(SurveysInProgress inProgSurvey){
		
		this.id =  inProgSurvey.getUser().getId().intValue();
		this.name = inProgSurvey.getUser().getFullName();
		this.complete = false;
		this.wasRequested = inProgSurvey.getWasRequested();
	}
	
	public RecipientResponse(SurveyCompletions completedSurvey){
		this.id =  completedSurvey.getUser().getId().intValue();
		this.name = completedSurvey.getUser().getFullName();
		this.complete = true;
		this.wasRequested = completedSurvey.getWasRequested();
	}
	
}
