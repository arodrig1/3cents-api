package feedback.response;

import feedback.model.QuestionOptions;

public class QuestionOptionResponse {

	private String optionText;
	private String optionValue;
	private String optionType;
	
	
	
	
	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getOptionText() {
		return optionText;
	}

	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public QuestionOptionResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public QuestionOptionResponse(QuestionOptions option){
		this.optionText = option.getOptionText();
		this.optionValue = option.getOptionValue();
		this.optionType = option.getOptionType();
	}
}
