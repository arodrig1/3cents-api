package feedback.response;

import java.util.List;

public class SurveyInstanceAnswerResponse {

	private Integer id;
	private String name;
	
	private List<AnswerResponse> answers;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AnswerResponse> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerResponse> answers) {
		this.answers = answers;
	}
	
	
}
