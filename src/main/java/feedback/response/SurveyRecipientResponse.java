package feedback.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("surveyInstance")
public class SurveyRecipientResponse {

	private Integer id;
	
	private List<RecipientResponse> recipients;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<RecipientResponse> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<RecipientResponse> recipients) {
		this.recipients = recipients;
	}
	
	
}
