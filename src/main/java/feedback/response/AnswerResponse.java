package feedback.response;

import feedback.model.Answers;
import feedback.model.Question;

public class AnswerResponse {

	private String questionId;
	private String answerText;
	
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	
	public AnswerResponse(Answers answer){
		
		this.answerText = answer.getAnswerText();
		this.questionId = answer.getQuestionId().toString();
	}
	
}
