package feedback.requests;

import java.util.List;

import feedback.model.Answers;
import feedback.response.AnswerResponse;

public class AnswerBatchRequest {

	private List<AnswerRequest> answers;
	
	private Boolean complete;
	
	

	public Boolean getComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public List<AnswerRequest> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerRequest> answers) {
		this.answers = answers;
	}
	
	
}
