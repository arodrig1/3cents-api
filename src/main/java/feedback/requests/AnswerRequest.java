package feedback.requests;

public class AnswerRequest {

	private Integer questionId;
	private String answerText;
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	public AnswerRequest() {
		super();
	}
	
	
}
