package feedback.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import feedback.model.SurveyInstances;

public interface SurveyInstanceRepository extends
		PagingAndSortingRepository<SurveyInstances, Long> {

}
