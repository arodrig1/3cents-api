package feedback.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import feedback.model.SurveysInProgress;

public interface SurveyInProgressRepository extends
		PagingAndSortingRepository<SurveysInProgress, Long> {

	List<SurveysInProgress> findBySurveyInstanceId(Integer surveyInstanceId );
	
	SurveysInProgress findByUserIdAndRecipientIdAndSurveyInstanceId(Integer userId, Integer recipientId, Integer surveyInstanceId);
}
