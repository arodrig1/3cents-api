package feedback.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import feedback.model.Users;

public interface UserRepository extends PagingAndSortingRepository<Users, Long> {

	
}
