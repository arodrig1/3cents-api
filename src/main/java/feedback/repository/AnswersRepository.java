package feedback.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import feedback.model.Answers;

public interface AnswersRepository extends PagingAndSortingRepository<Answers, Long> {

	
}
