package feedback.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import feedback.model.SurveyCompletions;

public interface SurveysCompletedRepository extends PagingAndSortingRepository<SurveyCompletions, Long>{

	List<SurveyCompletions> findBySurveyInstanceId(Integer surveyInstanceId );
	
	SurveyCompletions findByUserIdAndRecipientIdAndSurveyInstanceId(Integer userId, Integer recipientId, Integer surveyInstanceId);


}
