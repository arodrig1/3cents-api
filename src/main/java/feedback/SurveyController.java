package feedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedback.requests.AnswerBatchRequest;
import feedback.response.SurveyInstanceAnswerResponse;
import feedback.response.SurveyInstanceQuestionResponse;
import feedback.response.SurveyRecipientResponse;
import feedback.service.SurveyServiceExternal;
import feedback.service.UserServiceExternal;

@RestController
@RequestMapping(value = "/api/surveyInstance")
public class SurveyController {
    
	@Autowired
	UserServiceExternal userService;
	
	@Autowired
	SurveyServiceExternal surveyService;
	
    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }
    
    @RequestMapping("/testDb")
    public String getUser(){
    	return userService.testing();
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<?> getSurveyInstance(@PathVariable Integer id){
    	
    	
    	return null;
//    	return surveyService.getSurveyInstance(id);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/reciepients")
    public ResponseEntity<?> getSurveyRecipients(@PathVariable Integer id){
    	
    	SurveyRecipientResponse response = surveyService.getSurveyRecipients(id);
    	
    	return new ResponseEntity<SurveyRecipientResponse>(response, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/recipient/{id}/answers")
    public ResponseEntity<?> getSurveyAnswers(@PathVariable Integer id){
    	
    	
    	SurveyInstanceAnswerResponse response = surveyService.getSurveyAnswers(id);
    	return new ResponseEntity<SurveyInstanceAnswerResponse>(response, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/recipient/{id}/questions")
    public ResponseEntity<?> getSurveyQuestions(@PathVariable Integer id){
    	
    	SurveyInstanceQuestionResponse response = surveyService.getSurveyQuestions(id);
    	return new ResponseEntity<SurveyInstanceQuestionResponse>(response, HttpStatus.OK);

    }
    
    @RequestMapping(method = RequestMethod.PUT, value = "/{surveyId}/recipient/{id}/answers")
    public ResponseEntity<?> submitAnswersForSurvey(@PathVariable Integer surveyId, @PathVariable Integer id, @RequestBody AnswerBatchRequest request){
    	
    	surveyService.updateAnswers(request, id, 1, surveyId);
    	return new ResponseEntity<String>(HttpStatus.OK);
    }

}
