CREATE TABLE tblUsers (
  id INT(11) NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  fullName VARCHAR(255) NOT NULL,
  organizationId INT(11),
  managerId INT(11),
  userTypeId INT(11),
  password VARCHAR(255),
  enabled TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (id),
  UNIQUE KEY (email)
);

CREATE TABLE tblUserTypes (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tblOrganizations (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE tblSurveys (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tblSurveyInstances (
  id INT(11) NOT NULL AUTO_INCREMENT,
  surveyId INT(11),
  startTimestamp DATETIME,
  endTimestamp DATETIME,
  PRIMARY KEY (id),
  CONSTRAINT `fk_tblSurveyInstances_tblSurvey` FOREIGN KEY (surveyId) REFERENCES tblSurveys (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE tblQuestions (
  id INT(11) NOT NULL AUTO_INCREMENT,
  surveyId INT(11),
  questionTypeId INT(11),
  questionText VARCHAR(255),
  questionRangeStart INT(11),
  questionRangeEnd INT(11),
  PRIMARY KEY (id),
  CONSTRAINT `fk_tblQuestions_tblSurvey` FOREIGN KEY (surveyId) REFERENCES tblSurveys (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE tblQuestionTypes (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE tblQuestionOptions (
  id INT(11) NOT NULL AUTO_INCREMENT,
  questionId INT(11),
  optionText VARCHAR(255),
  optionValue VARCHAR(255),
  optionOrder INT(11),
  PRIMARY KEY (id),
  CONSTRAINT `fk_tblQuestionOptions_tblQuestions` FOREIGN KEY (questionId) REFERENCES tblQuestions (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE tblAnswers (
  id INT(11) NOT NULL AUTO_INCREMENT,
  userId INT(11),
  recipientId INT(11),
  surveyInstanceId INT(11),
  questionId INT(11),
  answerText VARCHAR(255),
  PRIMARY KEY (id),
  UNIQUE KEY (userId, recipientId, surveyInstanceId, questionId)
);

CREATE TABLE tblSurveyCompletions (
  id INT(11) NOT NULL AUTO_INCREMENT,
  userId INT(11),
  recipientId INT(11),
  surveyInstanceId INT(11),
  PRIMARY KEY (id)
);

CREATE TABLE tblSurveysInProgress (
  id INT(11) NOT NULL AUTO_INCREMENT,
  userId INT(11),
  recipientId INT(11),
  surveyInstanceId INT(11),
  wasRequested BIT(1),
  PRIMARY KEY (id)
);